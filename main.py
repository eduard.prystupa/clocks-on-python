import tkinter as tk
import time
import math
from tkinter import font


def update_clock():
    now = time.localtime()
    hours = now.tm_hour
    minutes = now.tm_min
    seconds = now.tm_sec

    hour_angle = -(hours % 12) * 30 - 0.5 * minutes + 90
    minute_angle = -minutes * 6 - 0.1 * seconds + 90
    second_angle = -seconds * 6 + 90

    canvas.delete("all")

    # drawning marker
    for i in range(1, 13):
        angle = -(i - 3) * 30  # The angular position of the number on the clock
        x = 150 + 100 * math.cos(math.radians(angle))
        y = 150 - 100 * math.sin(math.radians(angle))
        canvas.create_text(x, y, text=str(i), font=("Helvetica", 12, "bold"), fill="black")

    # Hour hand update
    hour_x = 150 + 60 * math.cos(math.radians(hour_angle))
    hour_y = 150 - 60 * math.sin(math.radians(hour_angle))
    canvas.create_line(150, 150, hour_x, hour_y, width=4, fill="black")

    # Minute hand update
    minute_x = 150 + 80 * math.cos(math.radians(minute_angle))
    minute_y = 150 - 80 * math.sin(math.radians(minute_angle))
    canvas.create_line(150, 150, minute_x, minute_y, width=2, fill="black")

    # Seconds hand update
    second_x = 150 + 90 * math.cos(math.radians(second_angle))
    second_y = 150 - 90 * math.sin(math.radians(second_angle))
    canvas.create_line(150, 150, second_x, second_y, width=1, fill="red")

    canvas.after(1000, update_clock)  # Planing update every second


root = tk.Tk()
root.title("Годинник зі стрілками та арабськими цифрами")

canvas = tk.Canvas(root, width=300, height=300, bg="white")
canvas.pack()

update_clock()

root.mainloop()
